<?php

/**
 * @link                https://dev-o-matic.com/
 * @since               0.1
 * @package             DBlaze/Plugin_Pack
 *
 * @wordpress-plugin
 * Plugin Name:         Davenport Blazers plugins pack
 * Plugin URI:          https://dev-o-matic.com/
 * Description:         Must-use plugins plugin pack
 * Version:             1.3.0
 * Author:              Dev-O-Matic
 * Author URI:          https://dev-o-matic.com/
 * License:             GPL-2.0+
 * License URI:         http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:         dom-dblaze-plugin-pack
 * Domain Path:         /languages
 */

require_once dirname( __FILE__ ) . "/plugins/woocommerce/woocommerce.php";
require_once dirname( __FILE__ ) . "/plugins/advanced-custom-fields-pro/acf.php";
require_once dirname( __FILE__ ) . "/plugins/woocommerce-max-quantity/woocommerce-max-quantity.php";
require_once dirname( __FILE__ ) . "/plugins/woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php";
require_once dirname( __FILE__ ) . "/plugins/payment-gateway-stripe-and-woocommerce-integration/stripe-payment-gateway.php";
require_once dirname( __FILE__ ) . "/plugins/yith-woocommerce-custom-order-status-premium/init.php";
require_once dirname( __FILE__ ) . "/plugins/yith-composite-products-for-woocommerce-premium/init.php";